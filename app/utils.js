import config from './config';

function log(msg){
	if(config.debug){
		console.log(msg);
	}
}

export { log }; 
