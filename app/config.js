
let config = {
	
	development: {

		debug: true,

		server: {
			port: 8443,
			keyPath: './tweedar.org.key',
			certPath: './tweedar.org.crt',
			passphrase: "w2V#cwaeb2@4f!jiju8n'9Jh?oh"
		},

		session: {
			maxAge: 1000*60*10	// 10 mins
		},

		twitter: {
			consumerKey: 'xPmOYnIrra1ZxJ90NbEwBQExc',
			consumerSecret: 'awio1GEHuTPQPbQlmcunrSYeMOrX7kEynujIr926QVwaZugMAo',
			callback: 'https://localhost:8444/callback' // set to the browsersync port
		}		

	},

	production: {

		debug: false,

		server: {
			port: 8443,
			keyPath: './tweedar.org.key',
			certPath: './tweedar.org.crt',
			passphrase: "w2V#cwaeb2@4f!jiju8n'9Jh?oh"
		},

		session: {
			maxAge: 1000*60*60*24*7
		},

		twitter: {
			consumerKey: 'xPmOYnIrra1ZxJ90NbEwBQExc',
			consumerSecret: 'awio1GEHuTPQPbQlmcunrSYeMOrX7kEynujIr926QVwaZugMAo',
			callback: 'https://localhost:8081/callback'
		}
		
	}

};

export default config[process.env.NODE_ENV];
