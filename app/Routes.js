
import express from 'express';
import path from 'path';
import twitter from './src/Twitter';

let Routes = express.Router();

Routes.get('/', (req, res)=>{

	if(!req.session.homepageVisited){
		res.render('index');
	} else {
		res.redirect('/map');
	}

/*	twitter.getRequestToken().then((json)=>{

		req.session.requestToken = json.requestToken;
		req.session.requestTokenSecret = json.requestTokenSecret;
		console.log(req.session);
		res.render('index');

	}).catch((err)=>{
		console.log(err);
	});*/
});

Routes.get('/request_token', (req, res)=>{
	res.redirect(`https://twitter.com/oauth/authenticate?oauth_token=${req.session.requestToken}`);
});

Routes.get('/callback', (req, res)=>{
	req.session.oauthToken = req.query.oauth_token;
	req.session.oauthVerifier = req.query.oauth_verifier;

	twitter.getAccessToken(req.session.requestToken, req.session.requestTokenSecret, req.session.oauthVerifier).then((json)=>{

		req.session.accessToken = json.accessToken;
		req.session.accessTokenSecret = json.accessTokenSecret;

		res.render('callback');

	}).catch((err)=>{
		console.log(err);
	});
});

Routes.get('/map', (req, res)=>{
	if(!req.session.homepageVisited){
		req.session.homepageVisited = true;
	}

	res.render('map');
});

Routes.post('/api/search/tweets', (req, res)=>{
	twitter.search(req.body, req.session.accessToken, req.session.accessTokenSecret).then((json)=>{
		res.json(json);
	}).catch((err)=>{
		console.log(err);
	});
});

export default Routes;

