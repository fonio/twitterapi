/* 
 * 
 */

import { exec } from 'child_process';
import TwitterAPI from 'node-twitter-api';
import config from '../config';
import { log } from '../utils';

class Twitter {

	constructor(){
		if(!Twitter.instance){
			this.config = config.twitter;
			this.api = new TwitterAPI({
				consumerKey: this.config.consumerKey,
				consumerSecret: this.config.consumerSecret,
				callback: this.config.callback
			});

			Twitter.instance = this;
		}

		return Twitter.instance;
	}

	getRequestToken(){
		return new Promise((resolve, reject)=>{
			this.api.getRequestToken(function(error, requestToken, requestTokenSecret, results){
				if (error) {
					console.log("Error getting OAuth request token : " + error);
					console.dir(error);
					reject(error);
				} else {
					//store token and tokenSecret somewhere, you'll need them later; redirect user 
					resolve({ requestToken, requestTokenSecret });
				}
			});
		});
	}

	getAccessToken(requestToken, requestTokenSecret, oauthVerifier){
		return new Promise((resolve, reject)=>{
			this.api.getAccessToken(requestToken, requestTokenSecret, oauthVerifier, (error, accessToken, accessTokenSecret, results)=>{
					if (error) {
					console.log("Error getting OAuth access token : " + error);
					console.dir(error);
					reject(error);
				} else {
					resolve({ accessToken, accessTokenSecret });
				}			
			});
		});
	}

	search(params, token, secret){

		log(params, token, secret);

		return new Promise((resolve, reject)=>{
			this.api.search(params, token, secret, function(err, data, res){
				if(err) {
					reject(err);
				} else {
					resolve(data);
				}
			});
		});
	}

}

// singleton
export default new Twitter();
