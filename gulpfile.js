
var gulp = require('gulp'),
	spawn = require('child_process').spawn,
	nodemon = require('gulp-nodemon'),
	browserSync = require('browser-sync').create(),
	exec = require('child_process').exec,
	babel = require("gulp-babel"),
	rollup = require("gulp-rollup"),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	runSequence = require('run-sequence'),
	webpack = require('webpack-stream');

require('babel-register');

// Bundlers

gulp.task('bundle-backend', function() {
	return gulp.src('app/main.js')
		.pipe(webpack({
			output: {
				filename: 'main.js',
			},
			target: "node",
			node: {
				__dirname: false
			},
			module: {
				loaders: [
					{
						test: /\.js$/,
						exclude: /(node_modules)/,
						loader: 'babel'
					},
					{
						test: /\.json$/,
						loader: 'json'
					}
				]
			},
		}))
		.pipe(uglify())
		.pipe(gulp.dest('dist/'));
});

gulp.task('bundle-frontend', ['sass'], function() {
	return gulp.src('www/js/main.js')
		.pipe(webpack({
			output: {
				filename: 'main.js',
			},
			module: {
				loaders: [
					{
						test: /\.js$/,
						exclude: /(node_modules)/,
						loader: 'babel'
					}
				]
			},
		}).on('error', function handleError() {
			this.emit('end'); // Recover from errors
		}))
		.pipe(uglify())
		.pipe(gulp.dest('www/dist/js'));
});

gulp.task('sass', function () {
	return gulp.src('www/css/**/*.scss')
		.pipe(sass({
			outputStyle: process.env.NODE_ENV === 'production' ? 'compressed' : 'nested'
		}).on('error', sass.logError))
		.pipe(gulp.dest('www/dist/css'));
});

gulp.task('set-dev-node-env', function() {
    return process.env.NODE_ENV = 'development';
});

gulp.task('set-prod-node-env', function() {
    return process.env.NODE_ENV = 'production';
});

// Production tasks
gulp.task('prod:restart', ()=>{
	runSequence('prod:stop', 'prod');
});

gulp.task('prod:stop', ['set-prod-node-env'], ()=>{
	let child = spawn('forever', ['stop', 'dist/main.js']);
	child.stdout.on('data', (data) => {
		console.log(data.toString());
	});
});

gulp.task('prod', ['set-prod-node-env', 'bundle-frontend', 'bundle-backend'], ()=>{

	// copy templates to dist for use by bundled application
	gulp.src(['app/views/**/*']).pipe(gulp.dest('dist/views'));

	let child = spawn('forever', ['start', 'dist/main.js']);
	child.stdout.on('data', (data) => {
		console.log(data.toString());
	});
});

// Developement task

gulp.task('dev', ['set-dev-node-env', 'bundle-frontend'], ()=>{

	let config = require('./app/config').default;

	nodemon({
		script: 'app/main.js',
		exec: 'babel-node',
		ignore: ['gulpfile.js', 'www/**/*']
	}).on('exit', ()=>{
		// nowt
	});

	browserSync.init({
		port: config.server.port +1,
		proxy: `http${config.server.keyPath && config.server.certPath ? 's' : ''}://localhost:${config.server.port}`,
		open: false,
		notify: false,
		https: config.server.keyPath && config.server.certPath
	});	

    gulp.watch(['www/*.html', 'www/css/**/*.css', 'app/views/**/*.hbs']).on('change', browserSync.reload);
    gulp.watch(['www/js/**/*.js', 'www/css/**/*.scss'], ['bundle-frontend', browserSync.reload]);
});
