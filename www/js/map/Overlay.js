
export default class Overlay {

	constructor($obj){
		this.instance = $obj;
		this.instance.addClass('overlay')

		if(this.instance.selector.length === 0){
			Overlay.container.append(this.instance);
		}
	}

	show(){
		this.instance.css('z-index', 100).addClass('show');
	}

	hide(){
		this.instance.removeClass('show').on('transitionend', (e)=>{
			if(e.target === this.instance[0] && 
				e.propertyName === 'opacity' &&
				!this.instance.hasClass('show')
			){
				this.instance.css('z-index', -1);
			}
		});
	}
}

Overlay.container = null;
