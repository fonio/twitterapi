import { EventSupportDecorator } from '../EventSupportDecorator';

export default class Marker {

	constructor(latlng){

		@EventSupportDecorator
		class GMarker extends google.maps.Marker {
		}

		let gmarker = new GMarker({
			position: { lat: latlng[0], lng: latlng[1] },
			clickable: true,
			icon: {
				//url: `data:image/svg+xml;utf-8,<svg version="1.2" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50" overflow="inherit"><path d="M25.015 2.4c-7.8 0-14.121 6.204-14.121 13.854 0 7.652 14.121 32.746 14.121 32.746s14.122-25.094 14.122-32.746c0-7.65-6.325-13.854-14.122-13.854z"/></svg>`,
				path: 'M25.015 2.4c-7.8 0-14.121 6.204-14.121 13.854 0 7.652 14.121 32.746 14.121 32.746s14.122-25.094 14.122-32.746c0-7.65-6.325-13.854-14.122-13.854z',
				scale: .5,
				fillOpacity: .25,
				fillColor: '#084A7F',
				strokeColor: '#555',
				strokeWeight: 1,
				anchor: new google.maps.Point(23, 48)
			}
		});

		// pass the click event up using event support
		gmarker.addListener('click', ()=>{
			gmarker.fireEvent('click');
		});

		return gmarker;
	}

}

