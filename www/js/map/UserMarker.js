import { EventSupportDecorator } from '../EventSupportDecorator';

@EventSupportDecorator
export default class UserMarker {

	constructor(opts){
		this.opts = opts;

		this.instance = new google.maps.Marker({
			position: this.opts.map.getCenter(),
			map: this.opts.map,
			draggable: true,
			icon: {
				path: 'M25.015 2.4c-7.8 0-14.121 6.204-14.121 13.854 0 7.652 14.121 32.746 14.121 32.746s14.122-25.094 14.122-32.746c0-7.65-6.325-13.854-14.122-13.854z',
				fillOpacity: .9,
				fillColor: '#D43D38',
				strokeColor: '#555',
				strokeWeight: 1,
				anchor: new google.maps.Point(23, 48)
			}
		});

		google.maps.event.addListener(this.instance, 'dragend', this.onMarkerMove.bind(this));
	}

	getPosition(){
		return this.instance.getPosition();
	}

	onMarkerMove(){
		this.fireEvent('marker-dragend', this);
	}
}
