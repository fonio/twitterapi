import { InstagramMedia, TwitterMedia, Media } from './Media';
import { EventSupportDecorator } from '../EventSupportDecorator';

@EventSupportDecorator
export default class Tweet {

	constructor(status){

		this.created_at = status.created_at;

		if(status.text.search(/https?:\/\//g) != -1){
			status.text.match(/https?:\/\/[^ ]*/g).forEach(function(url){
				status.text = status.text.replace(url, `<a href="${url}" target="_blank">${url}</a>`);
			});
		}

		this.instance = $(`
			<div class="tweet">
				<div class="created">
					<div class="inner"></div>
				</div>
				<div class="status">
					<div class="text">${status.text}</div>
					<div class="user">
						<img class="profile" src="${status.user.profile_image_url_https}" />
						${status.user.screen_name}
					</div>
				</div>
				<div class="media"></div>
			</div>
		`);

		this.setMedia(status);
		this.setMarker(status);

		this.updateCreatedAt();
		this.bindEvents();

		++Tweet.count;
	}

	bindEvents(){
		// mouse/touch events on the tweet instance
		['Click', 'Mouseover', 'Mouseout', 'Touchstart', 'Touchend', 'Touchmove'].forEach((event)=>{
			this.instance.on(event.toLowerCase(), this[`on${event}`].bind(this));
		});

		// on media image load
		$('.media img', this.instance).on('load', (e)=>{
			$(e.target).addClass('loaded');
		})
	}

	setMedia(status){
		if(status.entities.media && status.entities.media.length > 0){
			// twitter pics
			this.media = new TwitterMedia(status.entities.media[0].media_url_https);
		} else if(status.entities.urls && status.entities.urls.length > 0 && status.entities.urls[0].expanded_url.indexOf('instagram') != -1){
			// instagram pics
			this.media = new InstagramMedia(`${status.entities.urls[0].expanded_url}media/`);
		}

		if(this.media instanceof Media){
			this.instance.find('.media').append(`<img src="${this.media.getUrl(Media.SIZES.SMALL)}" alt="Small media image" />`);
		}
	}

	setMarker(status){
		let latlng = null;
		if(status.geo){
			latlng = status.geo.coordinates;
		} else if(status.place){
			var coords = status.place.bounding_box.coordinates[0];
			latlng = [ (coords[0][1] + coords[1][1] + coords[2][1] + coords[3][1]) / 4, 
				(coords[0][0] + coords[1][0] + coords[2][0] + coords[3][0]) / 4];
		} else {
			console.error('Tweet.setMarker', status);
		}

		this.marker = new Site.Marker(latlng);
		this.marker.on('click', this.onMarkerClick.bind(this));
	}

	setOverlay(){
		return new Promise((resolve, reject)=>{
			this.overlay = new Site.Overlay($(`
				<div class="tweet overlay">
					<div class="inner">
						<div class="full-image" style="background-image: url(${this.media.getUrl(Media.SIZES.LARGE)});"></div>
						<div class="close"></div>
					</div>
				</div>
			`));

			$('.close', this.overlay.instance).on('click touchstart', this.closeOverlay.bind(this));

			let lrgImg = document.createElement('img');
			lrgImg.src = this.media.getUrl(Media.SIZES.LARGE);
			lrgImg.onload = resolve;
			lrgImg.onerror = reject;
		});
	}

	onClick(){
		if(!this.media){
			return;
		}

		if(!this.overlay){

			Site.LoaderOverlay.show();

			this.setOverlay().then(()=>{
				Site.LoaderOverlay.hide();
				this.overlay.show();			
			}).catch((e)=>{
				Site.LoaderOverlay.hide();
				console.log('borked', e);
			});
		} else {
			this.overlay.show();
		}

		this.fireEvent('click', this);
	}

	closeOverlay(e){
		this.overlay.hide();
		this.fireEvent('close-overlay', this);
		return false;
	}

	onMarkerClick(){
		this.onClick();
	}

	onMouseover(e){
	}

	onMouseout(){
	}

	onTouchstart(){
		this.touchmoved = false;
	}

	onTouchmove(){
		this.touchmoved = true;
	}

	onTouchend(){
		if(!this.touchmoved){
			this.onClick();
		}
	}

	appendTo(id){
		this.instance.appendTo(id);
	}

	updateCreatedAt(){
		$('.created .inner', this.instance).html(moment(new Date(this.created_at)).fromNow());
	}
}

Tweet.count = 0;

