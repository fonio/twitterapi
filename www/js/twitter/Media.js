
export class Media {
	get urlSizeSmall() {}
	get urlSizeLarge() {}

	constructor(url){
		this.url = url;

		if(url.match(/^http:/)){
			console.warn('Insecure media being referenced, refusing to load', url);
			return {};
		}
	}

	getUrl(size){
		switch(size){
			case Media.SIZES.SMALL: return this.urlSizeSmall;
			case Media.SIZES.LARGE: return this.urlSizeLarge;
			default: return this.url;
		}
	}
}

Media.SIZES = { SMALL: 'SMALL', LARGE: 'LARGE' };

export class InstagramMedia extends Media {
	get urlSizeSmall() { return `${this.url}?size=t` }
	get urlSizeLarge() { return `${this.url}?size=l` }
}

export class TwitterMedia extends Media {
	get urlSizeSmall() { return `${this.url}:thumb` }
	get urlSizeLarge() { return `${this.url}:large` }
}
